Sysmon Configuration
====================

We are using [sysmon](https://puck.nether.net/sysmon/) to monitor the
anti-censorship team's infrastructure.  This repository contains our sysmon
configuration file.

 Sysmon runs checks every five minutes, and updates DNS records every ten
 minutes. If a check fails twice (e.g., a service is offline for more than five
 minutes), we get an alert.  Currently, cohosh and phw receive alerts.  Our
 sysmon instance currently does not have IPv6 capabilities.

Caveats
-------

gman999 and phw noticed that sysmon doesn't follow HTTP 301 redirects, so all
tests that rely on the `urltext` directive may be broken.  We discovered this
issue because sysmon didn't notice one of BridgeDB's outages: the Apache reverse
proxy (which sends the 301 redirect) was still online but BridgeDB's landing
page was not served. Sysmon's HTTP test is very simple and has no notion of HTTP
interaction.
